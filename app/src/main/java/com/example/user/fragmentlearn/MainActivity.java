package com.example.user.fragmentlearn;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.user.fragmentlearn.fragments.HospitalsFragment;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        policeStation();
        hospitals();
    }

    /*
    *   Events for main activity tab
    */
    private void policeStation(){
        Button policeStation = (Button) findViewById(R.id.policeStation);
        policeStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainer, HospitalsFragment.newInstance()).addToBackStack(null).commit();
                Toast.makeText( v.getContext(), "Police stations", Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void hospitals(){
        Button hospitals = (Button)findViewById( R.id.hospitals );
        hospitals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( v.getContext(), "Some Message", Toast.LENGTH_SHORT ).show();
            }
        });
    }

}

