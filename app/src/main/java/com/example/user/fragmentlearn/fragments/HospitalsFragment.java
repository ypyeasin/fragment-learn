package com.example.user.fragmentlearn.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.fragmentlearn.R;

public class HospitalsFragment extends Fragment {

    public static HospitalsFragment newInstance() {
        return new HospitalsFragment();
    }

    public HospitalsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hospitals, container, false);
    }

}
